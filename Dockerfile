FROM silex/emacs:28-ci as build
MAINTAINER Azrea Amis <atamiser@gmail.com>


# First sync is a workaround for https://github.com/doomemacs/doomemacs/issues/6960
RUN rm -fr ~/.emacs.d && git clone --depth 1 https://github.com/doomemacs/doomemacs ~/.emacs.d && \
	~/.emacs.d/bin/doom sync  || \ 
 	git -C ~/.emacs.d/.local/straight/repos/straight.el pull origin develop && \
	~/.emacs.d/bin/doom sync
